package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"crypto/x509"
	"io"
	"log"
	"sync"
	"github.com/quic-go/quic-go"
	"github.com/quic-go/quic-go/http3"
)

func main() {
	urls := []string{"https://localhost:5000"} // 요청을 보낼 서버 주소 리스트
	// 기타 설정
	insecure := false // 인증서 검증 비활성화 여부
	quiet := false    // 데이터 출력 여부

	pool, err := x509.SystemCertPool()
	if err != nil {
		log.Fatal(err)
	}

	roundTripper := &http3.RoundTripper{
		TLSClientConfig: &tls.Config{
			RootCAs:            pool,
			InsecureSkipVerify: insecure,
		},
		QuicConfig: &quic.Config{},
	}
	defer roundTripper.Close()
	hclient := &http.Client{
		Transport: roundTripper,
	}

	var wg sync.WaitGroup
	wg.Add(len(urls))
	for _, addr := range urls {
		log.Printf("GET %s", addr)
		go func(addr string) {
			rsp, err := hclient.Get(addr)
			if err != nil {
				log.Fatal(err)
			}
			log.Printf("Got response for %s: %#v", addr, rsp)

			body := &bytes.Buffer{}
			_, err = io.Copy(body, rsp.Body)
			if err != nil {
				log.Fatal(err)
			}
			if !quiet {
				log.Printf("Response Body:")
				log.Printf("%s", body.Bytes())
			}
			wg.Done()
		}(addr)
	}
	wg.Wait()
}

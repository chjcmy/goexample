import { createSignal } from "solid-js";

export default function Home() {

  const [message, setMessage] = createSignal("");

  async function fetchData() {
    try {
      const response = await fetch("https://127.0.0.1:4433/api/data", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
        quic: true, // Enable QUIC protocol
      });

      const data = await response.json();
      setMessage(data.message);
    } catch (error) {
      console.error("데이터를 가져오는 중 오류 발생:", error);
    }
  }

  return (
    <div>
      <h1>SolidJS와 Rust 서버 간의 HTTP/3 통신 예제</h1>
      <button onClick={fetchData}>데이터 가져오기</button>
      {message() && <p>서버로부터의 메시지: {message()}</p>}
    </div>
  );
}
